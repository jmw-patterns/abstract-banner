# About This Package
Though part of the Dudley Patterns Framework, this package is not intended
to be pulled directly into a project; rather, it gets included as a 
dependency by the modules that rely on it.

The Abstract Banner class supplies the base requirements needed for any 
type of banner. As such, custom actions, views, scripts, and styles are 
provided by the concrete classes.
