<?php
namespace Dudley\Patterns\Abstracts;

use Dudley\Patterns\Traits\ImageTrait;

/**
 * Class AbstractBanner
 *
 * @package Dudley\Patterns\Abstracts
 */
abstract class AbstractBanner extends AbstractPattern {
	use ImageTrait;

	/**
	 * AbstractBanner constructor.
	 *
	 * @param array  $img Image data.
	 * @param string $img_size Size of the image to render.
	 */
	public function __construct( $img, $img_size ) {
		$this->img      = $img;
		$this->img_size = $this->set_img_size( $img_size );
	}

	/**
	 * All banners require an image for output.
	 */
	public function requirements() {
		return [
			is_array( $this->img ),
		];
	}
}
